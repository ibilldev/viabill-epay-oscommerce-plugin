### osCommerce : ViaBill epay Payment module  ###
---------------------

ViaBill has developed a free payment module using Epay Payment Gateway which enables your customers to pay online for their orders in your Oscommerce Webshop.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
----
- version: 1.0
- Plugin on BitBucket (https://pdviabill@bitbucket.org/ibilldev/viabill-epay-oscommerce-plugin.git)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your OsCommerce Web Shop to provide a separate payment option to pay using ViaBill.

#REQUIREMENTS:
-----------
You must have the following:
- Oscommerce 2.3 based webshop (might also work with version 2.2)

- PHP version 5


  



###Integration Instructions###
-------------------------
Step 1

 Download the Module from the bitbucket. 

Step 2 

new files:
copy these files and folders to your webshop


	
	
	includes/modules/payment/viabillepaywindow.php	
	includes/modules/order_total/ot_viabillepay_transfee.php

    includes/languages/danish/modules/payment/viabillepaywindow.php	
    includes/languages/danish/modules/order_total/ot_viabillepay_transfee.php	
    
	includes/languages/english/modules/payment/viabillepaywindow.php
	includes/languages/english/modules/order_total/ot_viabillepay_transfee.php

	admin/viabillepay_handle_payment.php

	

##################################################################################### 


Step 3

Go into the Shop Administration->Modules->Payment.


Install the module "ViaBill ePay Payment Option".

#####################################################################################
  
#####################################################################################





##Uninstallation/Disable Module/Delete Module
-----------------------
1. Go to the Administration->Modules-> Payment
2. Look For "ViaBill ePay Payment Option"
3. Choose Option to Remove




#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)