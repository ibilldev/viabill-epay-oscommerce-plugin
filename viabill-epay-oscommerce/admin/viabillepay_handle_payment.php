<?php
/*
  Copyright (c) 2016. All rights reserved ViaBill - www.viabill.com

  This program is free software. You are allowed to use the software but NOT allowed to modify the software. 
  It is also not legal to do any changes to the software and distribute it in your own name / brand. 
*/

require('includes/application_top.php');

require(DIR_WS_INCLUDES . 'template_top.php');

function get_nb_code($code)
{
	switch($code)
	{
		case 208:
			return 'DKK';
			break;
		case 978:
			return 'EUR';
			break;
		case 840:
			return 'USD';
			break;
		case 578:
			return 'NOK';
			break;
		case 752:
			return 'SEK';
			break;
		case 826:
			return 'GBP';
			break;
		default:
			return 'DKK';
			break;
	}
}


if(($_POST['vbepay_capture'] OR $_POST['vbepay_move_as_captured'] OR $_POST['vbepay_credit'] OR $_POST['vbepay_delete']) AND isset($_POST['vbepay_transaction_id']))
{
	
	$client = new SoapClient('https://ssl.ditonlinebetalingssystem.dk/remote/payment.asmx?WSDL');
	
	if($_POST['vbepay_capture'])
	{
		$result = $client->capture(array
		(
			"merchantnumber" => MODULE_PAYMENT_VIABILLEPAYWINDOW_SHOPID,
			"transactionid" => $_POST["vbepay_transaction_id"],
			"amount" => (floatval($_POST["vbepay_amount"]) * 100),
			"pwd" => MODULE_PAYMENT_VIABILLEPAYWINDOW_API_PASSWORD,
			"pbsResponse" => - 1,
			"epayresponse" => - 1,
			)
		);
	}
	elseif($_POST['vbepay_credit'])
	{
		$result = $client->credit(array
		(
			"merchantnumber" => MODULE_PAYMENT_VIABILLEPAYWINDOW_SHOPID,
			"transactionid" => $_POST["vbepay_transaction_id"],
			"amount" => (floatval($_POST["vbepay_amount"]) * 100),
			"pwd" => MODULE_PAYMENT_VIABILLEPAYWINDOW_API_PASSWORD,
			"pbsresponse" => - 1,
			"epayresponse" => - 1,
			)
		);
	}
	elseif($_POST['vbepay_delete'])
	{
		$result = $client->delete(array
		(
			"merchantnumber" => MODULE_PAYMENT_VIABILLEPAYWINDOW_SHOPID,
			"transactionid" => $_POST["vbepay_transaction_id"],
			"pwd" => MODULE_PAYMENT_VIABILLEPAYWINDOW_API_PASSWORD,
			"epayresponse" => - 1,
			)
		);
	}
	elseif($_POST['vbepay_move_as_captured'])
	{
		$result = $client->moveascaptured(array
		(
			"merchantnumber" => MODULE_PAYMENT_VIABILLEPAYWINDOW_SHOPID,
			"transactionid" => $_POST["vbepay_transaction_id"],
			"pwd" => MODULE_PAYMENT_VIABILLEPAYWINDOW_API_PASSWORD,
			"epayresponse" => - 1,
			)
		);
	}
	
	if(@$result->captureResult == "true")
	{
		$message = "Capture OK";
	}
	elseif(@$result->creditResult == "true")
	{
		$message = "Credit OK";
	}
	elseif(@$result->deleteResult == "true")
	{
		$message = "Delete OK";
	}
	elseif(@$result->move_as_capturedResult == "true")
	{
		$message = "Moved OK";
	}
	else
	{		
		if($_POST['vbepay_capture'])
		{
			$pbsresponse = $result->pbsResponse;
		}
		elseif(!$_POST['vbepay_delete'] && !$_POST['vbepay_move_as_captured'])
		{
			$pbsresponse = $result->pbsresponse;
		}
		
		$message = "Error: Acquirer: " . $pbsresponse . " ViaBill ePay: " . $result->epayresponse;	
	}
	
	echo '<script type="text/javascript">alert("'. $message .'");</script>';;
}

?>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<?php
	$vbepay_transaction_query = tep_db_query("select vbcc_transactionid from " . TABLE_ORDERS . " where orders_id = '" . $_GET["oID"] . "'");
	while($vbepay_transaction = tep_db_fetch_array($vbepay_transaction_query))
	{
	?>
	<tr>
		<td width="100%">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="pageHeading"><?php echo $vbepay_transaction["vbcc_transactionid"]; ?></td>
					<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
					<td class="smallText" align="right"><?php echo tep_draw_button(IMAGE_BACK, 'triangle-1-w', 'orders.php?oID='. $_GET["oID"] .'&action=edit'); ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="main">
			<table>
				<tr>
					<td align="right">
						ePay Control Panel:
					</td>
					<td>
						<a href="https://ssl.ditonlinebetalingssystem.dk/admin/login.asp" title="ePay login" target="_blank">.../admin/login.asp</a>
					</td>
				</tr>
				<tr>
					<td align="right">
						Transaction ID:
					</td>
					<td>
						<b><?php echo $vbepay_transaction["vbcc_transactionid"] ?></b>
					</td>
				</tr>
				<?php
				if(MODULE_PAYMENT_VIABILLEPAYWINDOW_USE_API == "1")
				{
				?>
				<tr>
					<td colspan="2" class="main">
						<br />
						<?php
						
						$client = new SoapClient('https://ssl.ditonlinebetalingssystem.dk/remote/payment.asmx?WSDL');
						
						$vbepay_params = array();
						$vbepay_params['merchantnumber'] = MODULE_PAYMENT_VIABILLEPAYWINDOW_SHOPID;
						$vbepay_params['transactionid'] = $vbepay_transaction["vbcc_transactionid"];
						$vbepay_params["epayresponse"] = "-1";
						$vbepay_params["pwd"] = MODULE_PAYMENT_VIABILLEPAYWINDOW_API_PASSWORD;
						$soap_result = $client->gettransaction($vbepay_params);
						
						if(!$soap_result->transactionInformation->capturedamount or $soap_result->transactionInformation->capturedamount == $soap_result->transactionInformation->authamount)
						{
							$vbepay_amount = number_format($soap_result->transactionInformation->authamount / 100, 2, ".", "");
						}
						elseif($soap_result->transactionInformation->status == 'PAYMENT_CAPTURED')
						{
							$vbepay_amount = number_format(($soap_result->transactionInformation->capturedamount) / 100, 2, ".", "");
						}
						else
						{
							$vbepay_amount = number_format(($soap_result->transactionInformation->authamount - $soap_result->transactionInformation->capturedamount) / 100, 2, ".", "");
						}
						
						if($soap_result->transactionInformation->status != 'PAYMENT_DELETED' AND !$soap_result->transactionInformation->creditedamount)
						{
							?>
							<form name="viabill_epay_remote" action="<?php echo $_SERVER["REQUEST_URI"] ?>" method="post" style="display:inline">
								<input type="hidden" name="vbepay_transaction_id" value="<?php echo $vbepay_transaction["vbcc_transactionid"]; ?>" />
								<?php
									echo get_nb_code($soap_result->transactionInformation->currency);
									echo ' <input type="text" id="vbepay_amount" name="vbepay_amount" value="' . $vbepay_amount . '" size="' . strlen($vbepay_amount) . '" />';
								?>
								<?php
								if(!$soap_result->transactionInformation->capturedamount or ($soap_result->transactionInformation->transactionInformation->splitpayment and $soap_result->transactionInformation->status != 'PAYMENT_CAPTURED' and ($soap_result->transactionInformation->capturedamount != $soap_result->transactionInformation->authamount)))
								{
									echo ' <input class="button" name="vbepay_capture" type="submit" value="Capture" />';
									echo '<input class="button" name="vbepay_delete" type="submit" value="Delete" onclick="return confirm(\'Really want to delete?\');" />';
									
									if($soap_result->transactionInformation->splitpayment)
									{
										echo '<br /><input class="button" name="vbepay_move_as_captured" type="submit" value="Close transaction" /> ';
									}
									
								}
								elseif($soap_result->transactionInformation->status == 'PAYMENT_CAPTURED' OR $soap_result->transactionInformation->acquirer == 'EUROLINE')
								{
									echo ' <input class="button" name="vbepay_credit" type="submit" value="Credit" onclick="return confirm(\'Do you want to credit: ' . get_nb_code($soap_result->transactionInformation->currency) . ' \'+getE(\'vbepay_amount\').value);" />';
								}
								?>
							</form>
						<?php
						}
						else
						{
							echo get_nb_code($soap_result->transactionInformation->currency) . ' ' . $vbepay_amount;
							echo ($soap_result->transactionInformation->status == 'PAYMENT_DELETED' ? ' <span style="color:red;font-weight:bold;">Deleted</span>' : '');
						}
						
						?>
						<br /><br />
						<table class="table" cellspacing="0" cellpadding="2">
							<tr class="dataTableHeadingRow">
								<td class="dataTableHeadingContent">Date</td>
								<td class="dataTableHeadingContent">Event</td>
							</tr>
						
							<?php
							
							$historyArray = $soap_result->transactionInformation->history->TransactionHistoryInfo;
							
							if(!array_key_exists(0, $soap_result->transactionInformation->history->TransactionHistoryInfo))
							{
								$historyArray = array($soap_result->transactionInformation->history->TransactionHistoryInfo);
								// convert to array
							}
							
							for($i = 0; $i < count($historyArray); $i++)
							{
								echo "<tr class=\"dataTableRow\"><td class=\"dataTableContent\"><b>" . str_replace("T", " ", $historyArray[$i]->created) . "</b></td>";
								echo "<td class=\"dataTableContent\">";
								if(strlen($historyArray[$i]->username) > 0)
								{
									echo ($historyArray[$i]->username . ": ");
								}
								echo $historyArray[$i]->eventMsg . "</td></tr>";
							}
							
							?>
						</table>
					</td>
				</tr>
				<?php
				}
				?>
			</table>
		</td>
	</tr>
<?php
}
?>
</table>
<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>